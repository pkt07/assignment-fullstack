var express = require('express');
var path = require('path');
var http = require('http');
var createError = require('http-errors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var auth = require('./middlewares/auth');
var expressValidator = require('express-validator');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var debug = require('debug')('nodeapi:server');


var port = normalizePort(process.env.PORT || '5000');
var app = express();
var server = http.createServer(app);
server.on('error', onError);
server.on('listening', onListening);
server.listen(port, () => {
  console.log(`Server is up on ${port}`);
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true, limit: '1mb'}));

app.locals.ssetest = 'now';


//Handle CORS functionality

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader('Access-Control-Allow-Credentials', true)
  res.setHeader('Access-Control-Allow-Methods', "GET, POST, PUT, PATCH, DELETE");
  res.setHeader('Access-Control-Allow-Headers', "Content-Type, Authorization");
  next();
})



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('ssetest', 'defvalue');
//mongoose initialization 
mongoose.Promise = global.Promise;
//Seeding
mongoose.connect('mongodb://localhost/assignment')
  .then(() =>  console.log('connection successful'))
  .catch((err) => console.error(err));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Routes
var indexRouter = require('./routes/index');
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

module.exports = app;
