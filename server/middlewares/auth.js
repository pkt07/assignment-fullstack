var User = require('../src/models/User');
var jwt = require('jsonwebtoken');
const credentials = require('../config/credentials');

module.exports.isAuthorized  = function(req, res, next) {
	if(req.headers.authorization)
  	{
      if(req.headers.authorization==credentials.billerWebhookAuthHeader)
      {
        next();
      }
      else
      {
      	jwt.verify(req.headers.authorization, credentials.jwtSignKey, function(err, decoded) {
          if(err)
            return res.status(401).send({error: "wrong authorization header or authorization header expired"});
          if(decoded)
          {
            req.user={
              _id: decoded.id,
              restaurant_id: decoded.restaurant_id,
              permissions: decoded.permissions,
            };
            next();
          }
          else
          {
            return res.status(401).send({error: "you are not authorized, please log in first"});
          }
        });
      }
  	}
  	else
  	{
    	return res.status(401).send({error: "you are not authorized, please log in first"});
  	}
}


