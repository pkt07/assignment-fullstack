var express = require('express');
var router = express.Router();

let authController= require('../../src/controller/authController');

router.post('/register', authController.register);
router.post('/login', authController.login);
router.post('/profile', authController.profile);

module.exports.router = router;