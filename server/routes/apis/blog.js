var express = require('express');
var router = express.Router();

let blogController= require('../../src/controller/blogController');
const auth = require('../../middlewares/auth');

router.post('/postBlog', auth.isAuthorized,blogController.postBlog);
router.get('/blogs', auth.isAuthorized, blogController.blogs);
router.post('/ownBlog',auth.isAuthorized,blogController.ownBlog);
router.post('/upvote',auth.isAuthorized,blogController.upvote);

module.exports.router = router;