var express = require('express');
var router = express.Router();

const isAuthJwt = require('../../middlewares/auth');

router.get('/', function (req, res) {
    res.send("Welcome to APIs")
})

router.use('/auth', require('./auth').router);
router.use('/blog',require('./blog').router);

module.exports = router;