const response = {
    SUCCESS                   : 200,
    INVALID_PARAMETER         : 700,
    DATABASE_ERROR            : 701,
    FAILURE                   : 800,
    NODATA                    : 801,
    INVALID_ACTOR             : 802,
    INVALID_ASSET             : 803,
    NOT_ENOUGH_PARAMETERS     : 804,
    UNAUTHORISED              : 805
}

module.exports = response;