const mongoose = require('mongoose');
const config = require('config');

const uri_1 = config.get('mongodb_1.connecturi') + config.get('mongodb_1.db_name');
const uri_2 = config.get('mongodb_2.connecturi') + config.get('mongodb_2.db_name');

var connect_1;
var connect_2;

if (process.env.NODE_ENV == "production") {
    connect_1 = mongoose.createConnection(uri_1, {
        authSource: config.get('mongodb_1.authSource'),
        user: config.get('mongodb_1.user'),
        pass: config.get('mongodb_1.password'),
        replicaSet: config.get('mongodb_1.repSetName'),
        useNewUrlParser: true
    });

    connect_2 = mongoose.createConnection(uri_2, {
        authSource: config.get('mongodb_2.authSource'),
        user: config.get('mongodb_2.user'),
        pass: config.get('mongodb_2.password'),
        replicaSet: config.get('mongodb_2.repSetName'),
        useNewUrlParser: true
    });

} else {
    connect_1 = mongoose.createConnection(uri_1, {
        useNewUrlParser: true
    });
    connect_2 = mongoose.createConnection(uri_2, {
        useNewUrlParser: true
    });
}

exports.connect_mongodb_2 = connect_2
exports.connect_mongodb_1 = connect_1