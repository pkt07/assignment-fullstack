const Blog         = require("../models/Blog");
const RESPONSE     = require("../common/response");
const common       = require("../utility/commonHelper");
const _ 		   = require('lodash');

exports.postBlog = async (req, res) => {
   if(common.empty(req.body["name"])){
     res.json(common.missingParam("Name"));
    return 0;
  }

  if(common.empty(req.body["email"])) {
     res.json(common.missingParam("Email"));
    return 0;
  }

  if(common.empty(req.body["title"])) {
     res.json(common.missingParam("Title"));
    return 0;
  }
  if(common.empty(req.body["blogPost"])){
     res.json(common.missingParam("Post"));
     return 0;
  }
  let data = {
    name: req.body.name,
    email: req.body.email,
    title: req.body.title,
    blogPost: req.body.blogPost
  };
  var record = new Blog(data);

  record.save(function (error, data) {
    if (error) {
      res.json({
        response_code: RESPONSE.DATABASE_ERROR,
        response: "Database error"
      });
    } else {
      res.json({
        response_code: RESPONSE.SUCCESS,
        response: "Details submitted Successfully"
      });
    }
  });
};
exports.blogs = (req,res) =>{
	 Blog.find({}).sort({"createdAt":-1}).then(dt=>{
       res.json({code:200,status:'success',items:dt});
  })
};

exports.ownBlog = (req,res) =>{
	Blog.find({"email":req.body.email}).then(dt=>{
       res.json({code:200,status:'success',items:dt});
  })
}
exports.upvote = (req,res,next) =>{
	 if(common.empty(req.body["post_id"])) {
     res.json(common.missingParam("Post Id"));
    return 0;
  }
   if(common.empty(req.body["flag"])) {
     res.json(common.missingParam("Flag"));
    return 0;
  }
	 let flag = req.body.flag;
	 if(flag==0)
	 {
	 	Blog.findOneAndUpdate({"_id":req.body.post_id},{$inc:{upvotes:1}},{new:true},function(err,likes){
            if (err) return next(err);
            else{
            	res.json({code:200,status:'success',items:likes});
            }
        });

	 }
	 else{
	 	Blog.findOneAndUpdate({"_id":req.body.post_id},{$inc:{upvotes:-1}},{new:true},function(err,likes){
            if (err) return next(err);
            else{
            	res.json({code:200,status:'success',items:likes});
            }
        });
	 }
};