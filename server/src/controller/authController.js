const _ = require('lodash');
const jwt = require('jsonwebtoken');
const RESPONSE = require('../common/response');
const UserSocial = require("../models/User.js");
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const auth = require('../../middlewares/auth');
const handlebars= require('handlebars');
const credentials = require('../../config/credentials');
const common = require("../utility/commonHelper");

exports.register = async (req, res) => {
   if(common.empty(req.body["name"])){
     res.json(common.missingParam("Name"));
    return 0;
  }

  if(common.empty(req.body["email"])) {
     res.json(common.missingParam("Email"));
    return 0;
  }

  if(common.empty(req.body["phone"])) {
     res.json(common.missingParam("Mobile Number"));
    return 0;
  }
  if(common.empty(req.body["password"])){
     res.json(common.missingParam("Password"));
     return 0;
  }
 const A = await UserSocial.findOne({"email":req.body.email})
 if(_.isEmpty(A)){
 	bcrypt.hash(req.body.password, 10, function (err, hash) {
          if (err) console.log(err);
          req.body.password = hash;
          UserSocial.create(req.body, function(err, newuser){
            if(err) console.log(err);
            var jwtt = jwt.sign({id: newuser._id, name: newuser.name}, credentials.jwtSignKey);
            res.json({code: 200, status: 'success', message: 'Registered successfully', jwtt: jwtt, user: {name: newuser.name,phone:newuser.phone,email: newuser.email}});
          });
        });
 }
 else{
 	   res.json(common.error(RESPONSE.NODATA, 'User Already Register'));
 	   return 0;
 }
};

exports.login = async (req, res) => {
  if(common.empty(req.body["email"])) {
     res.json(common.missingParam("Email"));
    return 0;
  }
  if(common.empty(req.body["password"])){
     res.json(common.missingParam("password"));
     return 0;
  }
   UserSocial.authenticate(req.body.email.toLowerCase(), req.body.password, function (error, user) {
        if (error || !user) {
          res.status(401).send({code: 401, status: 'error', message: 'Wrong email or password'});
        }
        else 
        {
          var jwtt = jwt.sign({id: user._id, name: user.name}, credentials.jwtSignKey);
          res.json({code: 200, status: 'success', jwtt: jwtt, user: {_id: user._id, name : user.name,phone:user.phone, email : user.email,  imgname : user.imgname, profile : user.profile}});
        }
      });
};
exports.profile = (req,res) =>{
	 UserSocial.findOne({"email":req.body.email}).then(dt=>{
       res.json({code:200,status:'success',items:dt});
  })
};
