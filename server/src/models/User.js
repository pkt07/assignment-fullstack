var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var mongoosePaginate = require('mongoose-paginate');

var UserSocialSchema = new mongoose.Schema({
  name:{
    type: String,
    required: true 
  },
  email:{
    type: String,
    unique: true, 
    required: true,
    trim: true 
  },
  password:{
    type: String
  },
  phone:{
    type:Number
  },
  updated_at: {type: Date,default: Date.now},
  created_at: {type: Date, default: Date.now},
});

UserSocialSchema.statics.authenticate = function (email, password, next) {
  UserSocial.findOne({ email: email }).lean()
    .exec(function (err, user) {
      if (err) {
        return next(err);
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return next(err);
      }
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === true) {
          return next(null, user);
        } else {
          return next();
        }
      })
    });
}

UserSocialSchema.statics.authenticate_jwt = function (jwttoken, email, next) 
{
  UserSocial.findOne({ jwttoken: jwttoken }).exec(function (err, user) 
  {
      if (err) return next(err)
      else if (!user) 
      {
        var err = new Error('User not found.');
        err.status = 401;
        return next(err);
      }
      return next(null, user);
  });
}


UserSocialSchema.plugin(mongoosePaginate);

var UserSocial = mongoose.model('UserSocial', UserSocialSchema,'UserSocial');
module.exports = UserSocial;