var mongoose = require('mongoose');

var BlogSchema = new mongoose.Schema({
name:{
  type:String,
  required:true
},
email:{
  type:String,
  required:true
},
title:{
  type:String,
  required:true
},
blogPost:{
  type:String,
  required:true
},
upvotes:{
  type:Number,
  default:0
}
},{
    timestamps: {
        createdAt: true,
        updatedAt: true
  }   
});

module.exports = mongoose.model('Blog',BlogSchema,'Blog');
