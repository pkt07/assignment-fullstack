const RESPONSE = require("../common/response");
const moment   = require('moment');
exports.missingParam = (parameter) => {
	return {
		response_code: RESPONSE.NOT_ENOUGH_PARAMETERS,
		response: parameter + " missing"
	};
}

exports.success = (msg,data = '') => {
	return {
		response_code : RESPONSE.SUCCESS,
		response      : msg,
		data          : data
	};
}

exports.error = (err_code, msg) => {
	return {
		response_code: err_code,
		response: msg
	};
}
exports.sort_by = (field, reverse, primer)=>{

   var key = primer ? 
       function(x) {return primer(x[field])} : 
       function(x) {return x[field]};

   reverse = !reverse ? 1 : -1;

   return function (a, b) {
       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
     } 
}

exports.empty = (val) =>{
if (val === undefined)
    return true;

if (typeof (val) == 'function' || typeof (val) == 'number' || typeof (val) == 'boolean' || Object.prototype.toString.call(val) === '[object Date]')
    return false;
if (val == null || val.length === 0)   
    return true;
if (typeof (val) == "object") { 
    var r = true;
        for (var f in val)
            r = false;
        return r;
    }
    return false;
}
