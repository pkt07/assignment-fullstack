import React from 'react';  
import './App.css';  
import Login from './components/Login';   
import Reg from './components/Reg';  
import Dashboard from './components/Dashboard';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';   
function App() {  
  return (  
    <Router>    
      <div className="container">    
        <nav className="navbar navbar-expand-lg navheader">    
          <div className="collapse navbar-collapse" >    
            <ul className="navbar-nav mr-auto">      
            </ul>    
          </div>    
        </nav> <br />
         <Switch>    
          <Route exact path='/' component={Login} />   
          <Route path='/Signup' component={Reg} /> 
          <Route path='/Dashboard' component={Dashboard} /> 
        </Switch>   
      </div>    
    </Router>   
  );  
}  
export default App;