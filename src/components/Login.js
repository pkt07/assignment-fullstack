import React, { Component } from 'react';
import { Container, Row, Col } from 'react-grid-system';
import '../assets/css/login.css';  
import { Form} from 'reactstrap';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';   

class Login extends Component {
    constructor() {
        super();
        this.state = {
            Email: '',
            Password: ''
        }
        this.Password = this.Password.bind(this);
        this.Email = this.Email.bind(this);
        this.login = this.login.bind(this);
    }
    Email(event) {
        this.setState({ Email: event.target.value })
    }
    Password(event) {
        this.setState({ Password: event.target.value })
    }
    login(event) {
        fetch('https://blopper-app.herokuapp.com/api/auth/login', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: this.state.Email,
                password: this.state.Password
            })
        }).then((Response) => Response.json())
            .then((result) => {
                console.log(result);
                if (result.code != 200)
                    alert('Invalid User');
                else{
                    localStorage.setItem("token", result.jwtt);
                    localStorage.setItem("email", result.user.email);
                    this.props.history.push("/Dashboard");
                }
            })
    }
    render() {
        return (
   <Container className="myContainer">
  <Row className="my-row" style={{marginLeft:"0px",width:"105%"}}>
    <Col md={8}>
     <img className = "img-left" alt="deskup" src = {require("../assets/images/side-image.jpeg")} height="340px" width="550px"/>   
    </Col>
    <Col md={4}>
         <img className = "img-right" alt="Coworking" src = {require("../assets/images/right-side.png")} height="570px" width="460px"/>   
    	<div className ="form-login">
    		<Form className="mylogin">
    			<h3 className="form-head">CUSTOMER LOGIN</h3>
			
                <div className="log">
                    <div className="content">
                    <div className="l-2">
                        <i className="fa fa-envelope icon-style"></i><input type="email" id="email" placeholder="Email Id" onChange={this.Email} />
                        <i className="fa fa-lock icon-style"></i><input type="password" id="password" placeholder="Password" onChange={this.Password} />    
                    </div>
                   <div className="combine">
                   	 <Col md={6}>
                    	<div className="input-box"><input type="checkbox" checked/><p className="para-remember">Remember Me</p></div>
				    </Col>
  					<Col md={6}>
  						<p className="para-forget">Forget Password ?</p>
  					</Col>
                   </div>
                    <div className="button">
                        <div className="signup" onClick={this.login}>LOGIN</div>
                        <p className="or-para">OR</p>
                         <Link to={'/Signup'} className="nav-link">Sign Up</Link>    
                    </div>
                </div>
            </div>
            </Form>
    	</div>
    </Col>

  </Row>
</Container>
  );
    }
}
export default Login;

            