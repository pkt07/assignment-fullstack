import React, { Component } from 'react';
import { Container, Row, Col } from 'react-grid-system';
import '../assets/css/dashboard.css';  
import Popup from "reactjs-popup";
import { BrowserRouter as  Link } from 'react-router-dom';   

import { Form} from 'reactstrap';

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            profile:'',
            blogs:'',
            Title:'',
            BlogPost:''
        }
        	this.Title = this.Title.bind(this);
	        this.BlogPost = this.BlogPost.bind(this);
	        this.postBlog = this.postBlog.bind(this);
            this.handleClick = this.handleClick.bind(this);
            this.Logout = this.Logout.bind(this);
    }
    Title(event) {
        this.setState({ Title: event.target.value })
    }
    BlogPost(event) {
        this.setState({ BlogPost: event.target.value })
    }
    postBlog(event) {
        fetch('https://blopper-app.herokuapp.com/api/blog/postBlog', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':localStorage.getItem('token')
            },
            body: JSON.stringify({
                title: this.state.Title,
                blogPost: this.state.BlogPost,
                name:this.state.profile.name,
                email:localStorage.getItem('email')
            })
        }).then((Response) => Response.json())
            .then((result) => {
                console.log(result);
                if (result.response_code != 200)
                    alert('Error Occured');
                else{
                	alert('Submitted Successfully');
                    window.location.reload();
                   ;
                }
            })
    }
    Logout(event) {
        localStorage.clear();
        window.location.href="/"

    }
    handleClick = (event,post_id,str) => {
    	var flag;
    	if(str=='up'){
    		flag=0;
    	}
    	else
    		flag=1;
    fetch('https://blopper-app.herokuapp.com/api/blog/upvote', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':localStorage.getItem('token')
            },
            body: JSON.stringify({
                post_id: post_id,
                flag: flag
            })
        }).then((Response) => Response.json())
            .then((result) => {
                if (result.code != 200)
                    alert('Error Occured');
                else{
                	 window.location.reload();
                    this.props.history.push("/Dashboard");
                }
            })
  	}
    componentWillMount() {
    	fetch('https://blopper-app.herokuapp.com/api/auth/profile', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: localStorage.getItem('email')
            })
        }).then((Response) => Response.json())
            .then((result) => {
            	this.setState({ profile:result.items});
            })

        fetch('https://blopper-app.herokuapp.com/api/blog/blogs', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':localStorage.getItem('token')
            }
        }).then((Response) => Response.json())
            .then((result) => {
            	this.setState({ blogs:result.items});
            })
    }
    render() {
    	const data = this.state.profile;
    	const blogs = this.state.blogs;
    	var myProfile = [];
    	var myBlogs = [];
    	var blogList = [];
    	myProfile.push(<h2 className="header-div">Hello {data.name} </h2>)
    	for(let i=0;i<blogs.length;i++){
    		myBlogs.push(
    				
    				<div className="blogs-t">
    				<Row className="my-row" style={{marginLeft:"0px"}}>
    				<Col md={6}>
    					<h4 className="created-by">{blogs[i].name.toUpperCase()}</h4>
    				</Col>
    				<Col md={6}>
    					<h4 className="created-at">{blogs[i].createdAt}</h4>
    				</Col>
    				</Row>
    					<p className="title-head"><center>{blogs[i].title.toUpperCase()}</center></p>
    					<p className="para-content">{blogs[i].blogPost}</p>
    					<hr className="hr-tag"/>
    					<Row className="my-row" style={{marginLeft:"0px"}}>
    						<Col md={4}>
    						<p className="upvotes">Total upvotes:{blogs[i].upvotes}</p>
    						</Col>
    						<Col md={4}>
    						<button className="upbutton" onClick={(event)=> {this.handleClick(event,blogs[i]._id,"up")}}><i className="fa fa-arrow-up icon-up">Upvote</i></button>
    						</Col>
    						<Col md={4}>
    						<button className="upbutton" onClick={(event)=> {this.handleClick(event,blogs[i]._id,"down")}}><i className="fa fa-arrow-down icon-up" >Downvote</i></button>
    						</Col>
    					</Row>
    					<br/>
    				</div>

    			)
    	}
    	if(myBlogs.length>0){
    		blogList = myBlogs;
    	}
    	else
    	{
    		blogList.push(<p>You have to add Post to see them Listed Here</p>)
    	}

        return (
        	<div>
        	<div className="nav-bar">
        		<a className="link-nav" href="#">Blopper:A place to Blog</a>
        		<a className="link-nav" href="#" style={{position:"relative",float:"right"}} onClick={this.Logout}>Logout</a>
        	</div>
        	 <Container className="myContainer" style={{position:"relative",marginTop:"2%"}}>
        	 <Row className="my-row" style={{marginLeft:"0px",width:"105%"}}>
			    <Col md={3}>
			     {myProfile}
			    </Col>
			    <Col md={6}>
			     {
			     	blogList
			     }
			    </Col>
			    <Col md={3}>
			   <Popup trigger={<button className="button type-2"> <i className="fa fa-plus"></i>Add Post</button>} modal>
    {close => (
      <div className="modal">
        <a className="close" onClick={close}>
          &times;
        </a>
        	<div className ="form-login">
    		<Form style={{paddingTop:"12%",paddingBottom:"5%",position:"relative",left:"-8%"}}>
    			<h3 className="form-head">Add your Own Post</h3>
			
                <div className="log">
                    <div className="content">
                    <div className="l-2">
                        <input type="text" id="title" placeholder="Title of your blog" onChange={this.Title} />
                        <textarea rows="4" cols="65"  placeholder="Type Your Content here" onChange={this.BlogPost} />    
                    </div>
                    <div className="button" style={{marginLeft:"0px",width:"100%",color:"#333",fontWeight:"bold"}}>
                        <div className="signup" onClick={this.postBlog}>Submit</div>  
                    </div>
                </div>
            </div>
            </Form>
    	</div>
      
        
      </div>
    )}
  </Popup>
			    </Col>
			  </Row>
        	 </Container>
        	 </div>
  );
    }
}
export default Dashboard;