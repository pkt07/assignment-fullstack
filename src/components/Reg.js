  import React, { Component } from 'react';
  import { Container, Row, Col } from 'react-grid-system';
  import '../assets/css/regis.css';    

  import { Form} from 'reactstrap';

  class Reg extends Component {
  constructor() {
    super();
    this.state = {
      Name: '',
      Email: '',
      Phone:'',
      Password: ''
    }
    this.Email = this.Email.bind(this);
    this.Password = this.Password.bind(this);
    this.Name = this.Name.bind(this);
    this.Phone = this.Phone.bind(this);
    this.register = this.register.bind(this);
  }
  Email(event) {
    this.setState({ Email: event.target.value })
  }
  Password(event) {
    this.setState({ Password: event.target.value })
  }
  Name(event) {
    this.setState({ Name: event.target.value })
  }
  Phone(event){
    this.setState({ Phone: event.target.value })
  }
  register(event) {
    fetch('http://192.168.43.26:5000/api/auth/register', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.Name,
        password: this.state.Password,
        email: this.state.Email,
        phone: Number(this.state.Phone),
      })
    }).then((Response) => Response.json())
      .then((Result) => {
        if (Result.code == 200){
          localStorage.setItem("token", Result.jwtt);
          localStorage.setItem("email", Result.user.email);
          this.props.history.push("/Dashboard");
        }        
        else
          alert('Sorrrrrry !!!! Un-authenticated User !!!!!')
      })
  }
    render() {
              return (
   <Container className="myContainer">
  <Row className="my-row" style={{marginLeft:"0px",width:"105%"}}>
    <Col md={8}>
     <img className = "img-left" alt="deskup" src = {require("../assets/images/side-image.jpeg")} height="340px" width="550px"/>   
    </Col>
    <Col md={4}>
         <img className = "img-right" alt="Coworking" src = {require("../assets/images/right-side.png")} height="570px" width="460px"/>   
      <div className ="form-login">
        <Form className="mylogin">
          <h3 className="form-head" style={{fontSize:"1.2em"}}>CUSTOMER SIGNUP</h3>
      
                <div className="log">
                    <div className="content">
                    <div className="l-2">
                        <i className="fa fa-users icon-style"></i><input type="text" id="name" placeholder="Name" onChange={this.Name} />
                        <i className="fa fa-envelope icon-style"></i><input type="email" id="email" placeholder="Email Id" onChange={this.Email}/>
                        <i className="fa fa-phone icon-style"></i><input type="phone" id="phone" placeholder="Phone" onChange={this.Phone} />
                        <i className="fa fa-lock icon-style"></i><input type="password" id="password" placeholder="Password" onChange={this.Password}/>    
                    </div>
                  <div className="button type-2">
                          <div className="signup" onClick={this.register}>Confirm</div> 
                </div>
                </div>
            </div>
            </Form>
      </div>
    </Col>

  </Row>
</Container>
  );
    }
  }
  export default Reg;