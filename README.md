# assignment-fullstack 

All folder except server are client side folder.
Server folder contains all code of backend.

In this assignment backend side has been developed in Nodejs,Expressjs and Mongodb as Database.Source code of the same has been placed in Server folder.
Frontend Side has been developed in reactjs.
#### Overview and Working
####### Basic idea was to make user Signup/Login into the system so that he/she can add blogpost by their name.
######### So the app flows goes like this
* User Login/Signup(Signup for new user) .
* After Login user redirects to /dashboard
* In dashboard user can add blogs , upvotes/downvotes existing blogs,see total upvotes too.
* Blogs listing contains the name of creator,time of creation,title of blog,main content and total upvotes.

Its a reactjs application with port specified as 3000.

#### Backend codestructure 
* Backend code will run on port 5000 and it follows MVC architecture with jwt token.
* Routes are defined in routes folder and models and controller are defined in src folder.
* Src folder also contain response code and utility file which contain user defined(created by me) functions.

######## Both client and server can be run through npm start

Currently application is deployed at [https://blopper-front.herokuapp.com/] and it uses api which is deployed at 
[https://blopper-app.herokuapp.com/]

###### Screenshot of the app:

#### Login Page
![picture](screenshot/Login.png)

#### Singup Page
![picture](screenshot/signup.png)

#### Dashboard when its idle
![picture](screenshot/idle_dashboard.png)

#### Add post page
![picture](screenshot/add_post.png)

#### Dashboard with lists of blogs
![picture](screenshot/dashboard_blog.png)
